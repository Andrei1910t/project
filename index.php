<?php

require __DIR__ . '/vendor/autoload.php';

$queryString = $_SERVER["REQUEST_URI"];
$url = parse_url($_SERVER['REQUEST_URI']);

session_start();

$router = new \AYG\Router();
$router->addRoute([
    'controller' => AYG\Controllers\ListController::class,
    'url' => '/listcontroller',
    'method' => 'displayDB'
]);
$router->addRoute([
    'controller' => AYG\Controllers\DeleteController::class,
    'url' => '/deletecontroller',
    'method' => 'delete'
]);
$router->addRoute([
    'controller' => AYG\Controllers\AddController::class,
    'url' => '/addcontroller',
    'method' => 'add'
]);
$router->addRoute([
    'controller' => AYG\Controllers\ReplaceController::class,
    'url' => '/replacecontroller',
    'method' => 'replace'
]);

$router->dispatch($queryString);

