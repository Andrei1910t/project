<?php

namespace AYG\Controllers;

use AYG\Models\Model;
use AYG\Views\ListView;

class ListController 
{
    protected $model;
    protected $view;

    public function displayDB()
    {
        $this->model = new Model();
        $this->view = new ListView();
        $personsArray = [];
        try {
            $personsArray = $this->model->returnDataDB();
            $this->view->display($personsArray);
        } catch (\Exception $e) {
            print($e->getMessage());
        }
    }
}

?>