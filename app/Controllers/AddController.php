<?php

namespace AYG\Controllers;

use AYG\Models\Model;

class AddController
{
    protected $model;
    public function add()
    {
        if (isset($_POST['first_name']) || isset($_POST['last_name']) || isset($_POST['email']))
        {
            $firstName = $_POST['first_name'];
            $lastName = $_POST['last_name'];
            $email = $_POST['email'];
            $this->model = new Model();
            if($this->model->fieldValidation($firstName, $lastName, $email) === false)
            {
                if ($this->model->nameValidation($firstName) === FALSE 
                || $this->model->nameValidation($lastName) === FALSE
                || $this->model->emailValidation($email) === FALSE ) {
                    try {
                        $this->model->addDataDB($firstName, $lastName, $email);
                        http_response_code(200);
                        echo json_encode(['error' => false, 'message' => 'User added']);
                    } catch (\Exception $e) {
                        echo json_encode(['error' => true, 'message' => 'error']);
                    }
                } else {
                    echo json_encode(['error' => true, 'message' => 'Invalid name']);
                }
            } else {
                echo json_encode(['error' => true, 'message' => 'All fields required']);
            }
        }
    }
}