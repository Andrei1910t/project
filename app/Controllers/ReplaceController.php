<?php

namespace AYG\Controllers;

use AYG\Models\Model;

class ReplaceController
{
    public function replace()
    {
        if (isset($_POST['first_name']) || isset($_POST['last_name']) || isset($_POST['email']))
        {
            $firstName = $_POST['first_name'];
            $lastName = $_POST['last_name'];
            $email = $_POST['email'];
            $id = $_POST['id'];
            $this->model = new Model();
            if($this->model->fieldValidation($firstName, $lastName, $email) === false)
            {
                try {
                    $this->model->editDataDB($id, $firstName, $lastName, $email);
                    echo json_encode(['error' => false, 'message' => 'User edited successfully']);
                } catch (\Exception $e) {
                    print($e->getMessage());
                    echo json_encode(['error' => true, 'message' => 'User could not be edited']);
                }
            } else {
                echo json_encode(['error' => true, 'message' => 'All fields required!']);
            }
        }

    }
}