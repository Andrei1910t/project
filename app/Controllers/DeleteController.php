<?php

namespace AYG\Controllers;

use AYG\Models\Model;

class DeleteController
{
    protected $model;
    public function delete()
    {
        if (isset($_POST['id']))
        {
            $id = $_POST['id'];
            $this->model = new Model();
            try {
                $this->model->deleteDataDB($id);
                echo json_encode(['error' => false, 'message' => 'User deleted successfully']);
            } catch (\Exception $e) {
                print($e->getMessage());
                echo json_encode(['error' => true, 'message' => 'User not deleted']);
            }
        }
        
    }

}

?>