<?php

namespace AYG\Models;

class Model
{
    protected $mysqli;

    public function __construct()
    {
        define('DB_SERVER', 'localhost');
        define('DB_USERNAME', 'andrei1910');
        define('DB_PASSWORD', 'nustiu2012');
        define('DB_NAME', 'TabelDB');
        $this->mysqli = new \mysqli(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
    }
        
    public function returnDataDB()
    {
        $sql = [];
        $sql = mysqli_query($this->mysqli, "SELECT * FROM persons");
        return $sql;
    }

    public function deleteDataDB($id)
    {
        $stmt = $this->mysqli->prepare('DELETE FROM persons WHERE id = ?');
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
    }

    public function addDataDB($firstName, $lastName, $email)
    {
        $stmt = $this->mysqli->prepare("INSERT INTO persons (first_name, last_name, email)
                                                    VALUES (?, ?, ?)");
        $stmt->bind_param("sss", $firstName, $lastName, $email);
        $stmt->execute();
        $stmt->close();
        
    }

    public function editDataDB($id, $firstName, $lastName, $email)
    {
        $stmt = $this->mysqli->prepare("UPDATE persons SET first_name = ?, last_name = ?, email = ?
                                                     WHERE id = ? ");
        $stmt->bind_param("sssi", $firstName, $lastName, $email, $id);
        $stmt->execute();
        $stmt->close();
    }

    public function nameValidation($name)
    {
        if (preg_match("/^[a-zA-Z ]*$/",$name)) {
            return false;
        } else {
            return true;
        }
    }

    public function emailValidation($email)
    {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                return false;
            } else {
                return true;
            }
    }

    public function fieldValidation($firstName, $lastName, $email)
    {
        $required = array($firstName, $lastName, $email);
        $error = false;
        foreach($required as $field) {
            if (empty($field)) {
            return true;
            }
        }
            return $error;
    }
    
}

?>