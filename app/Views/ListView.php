<?php

namespace AYG\Views;

use AYG\Controllers\ListController;

Class ListView
{
    public function display($personsArray)
    {
        $loader = new \Twig_Loader_Filesystem($_SERVER['DOCUMENT_ROOT'].'/templates');
        $twig = new \Twig_Environment($loader);
        echo $twig->render('list.html.twig', ['personsArray' => $personsArray] );
    }
}

?>