<?php

namespace AYG;

use AYG\Controllers\NotFound;

class Router
{
    private $routes = [];

    public function getRoutes()
    {
        return $this->routes;
    }

    public function addRoute($route)
    {
        $this->routes[] = $route;
    }

    public function dispatch($url)
    {
        $existingRoute = false;
        foreach ($this->getRoutes() as $route) {
            if ($route['url'] == $url) {
                $existingRoute = $route;
                break;
            }
        }
        if ($existingRoute !== FALSE) {
            $controllerClass = $existingRoute['controller'];
            $controller = null;
                $controller = new $controllerClass();
                $controller->{$existingRoute['method']}();
        } else {
            $this->controller = new NotFound();
        }
    }
}

?>