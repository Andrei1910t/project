$(document).ready(function(){

    var fields = 
        '<tr id = slide-down>' +
            '<td></td>'+
            '<td class="add-container"><input type="text" name="first_name" id="first_name" value="" required></td>' +
            '<td class="add-container"><input type="text" name="last_name" id="last_name" value="" required></td>' + 
            '<td class="add-container"><input type="text" name="email" id="email" value="" required></td>' + 
            '<td class="add-container"><button class="btn save-button" type="submit" name="id" value="value"> Save</button></td>'+
        '</tr>';
    
    var edit =
    '<div class="form-popup" id="myForm">' +
    '<div class="form-container">'+
        '<h1>Edit</h1>'+
        '<label for="firstname"><b>First Name</b></label>'+
        '<input type="text" name="firstname" required>'+
        '<label for="lastname"><b>Last Name</b></label>'+
        '<input type="text" name="lastname" required>'+
        '<label for="email"><b>Email</b></label>'+
        '<input type="text" name="email" required>'+
        '<input type="hidden" name="id">'+
        '<button type="submit" class="btn save-edit">Save</button>'+
        '<button type="button" class="btn cancel">Close</button>'+
        '</div>'+
    '</div>';

    $(".submit-button").click(function(){
        $('#user-table tbody:last-child').append(fields);
        $("#slide-down").hide().fadeIn(500);
        $(".td-hide").hide();
    });

    $(".delete-button").click(function(event){
        var userID = $(this).data("user-id");
        $.ajax({
            method: "POST",
            url: "/deletecontroller",
            data: { id: userID },
            dataType: "json"
          })
            .done(function(data) {
                if (data.error == false) {
                    alert(data.message);
                    location.reload();
                } else {
                    alert(data.message);
                    location.reload();
                }
            });
        event.preventDefault();
    });

    $(".edit-button").click(function(event){
        $('#user-table').append(edit);
        event.preventDefault();
    });

    $(".edit-button").click(function(event){
        var $currentTarget = $(event.currentTarget),
            id = $currentTarget.data("user-id"),
            firstName = $currentTarget.data("user-first"),
            lastName = $currentTarget.data("user-last"),
            email = $currentTarget.data("user-email");

        var $popup = $("#myForm");
        $popup.show();
        $popup.find('input[name="id"]').val(id);
        $popup.find('input[name="firstname"]').val(firstName);
        $popup.find('input[name="lastname"]').val(lastName);
        $popup.find('input[name="email"]').val(email);
    });
    
    $(document).on("click", ".cancel",function(){
        var $popup = $("#myForm");
        $popup.hide();
    });

    $(document).on("click", ".save-edit",function(){
        var id = $("input[name=id]").val();
        var firstName = $("input[name=firstname]").val();
        var lastName = $("input[name=lastname]").val();
        var email = $("input[name=email]").val();
        $.ajax({
            method: "POST",
            url: "/replacecontroller",
            data: { id: id, first_name: firstName, last_name: lastName, email: email },
            dataType: "json"
          })
            .done(function(data) {
                if (data.error == false) {
                    alert(data.message);
                    location.reload();
                } else {
                    alert(data.message);
                }
            });
    });

    $(document).on("click",".save-button",function(event){ 
        var firstName = $('#first_name').val();
        var lastName = $('#last_name').val();
        var email = $('#email').val();
        $.ajax({
            method: "POST",
            url: "/addcontroller",
            data: { first_name: firstName, last_name: lastName, email: email },
            dataType: "json"
          })
            .done(function(data) {
                if (data.error == false) {
                    alert(data.message);
                    location.reload();
                } else {
                    alert(data.message);
                    location.reload();
                }
            })
            //.fail(function(err) {
              //  location.reload();
            //});
	});

});